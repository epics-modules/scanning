/*!
 * \file concatTSArray.c
 * \brief Concatenate timestamps
 * \author Alexander Söderqvist <alexander.soderqvist@cosylab.com>
 */

#include <stdio.h>
#include <string.h>
#include <errlog.h>

#include <epicsTypes.h>
#include <aSubRecord.h>
#include <registryFunction.h>
#include <epicsExport.h>

/*!
 * This functions run at IOC init. It checks if the input is a scalar
 *
 * \param[in,out] precord initialized record.
 */
static long concatTSArrayInit(aSubRecord *precord)
{
	epicsUInt32 tsSize = precord->noa;

	if(tsSize != 1){
	  	errlogPrintf("Number of elements in inpA is out of range."
			     "NOA=%d, only takes a scalar\n", tsSize);
		return -1;
	}
	
	
	return 0;
}

/*!
 * This functions runs everytime the record is processed. It puts a
 * timestamp in string array with all timestamps from the different scan points
 * 
 * \param[in,out] precord processed record.
 */
static long concatTSArrayProcess(aSubRecord *precord)
{	
	
	epicsOldString* timestamp = (epicsOldString*)(precord->a);	
	epicsInt32* tsIndex = (epicsInt32*)(precord->b);

	epicsOldString* timestampArray = (epicsOldString*)(precord->vala);
	epicsUInt32* tsArrayNelm = &(precord->neva);
	epicsUInt32 arraySize = precord->nova;

	/*Check if timestamp index is within range*/
	if(*tsIndex >= arraySize || *tsIndex < 0){
		errlogPrintf("ERROR: tsIndex is out of bounds, tsIndex is %d"
			     " and arraySize is %u\n", *tsIndex, arraySize);
		return -1;
	}

	strcpy(timestampArray[*tsIndex], *timestamp);
	*tsArrayNelm = *tsIndex + 1;
	
	return 0;
}

/* Register these symbols for use by IOC code: */
epicsRegisterFunction(concatTSArrayInit);
epicsRegisterFunction(concatTSArrayProcess);
