# File  : scanTRDetTrig.template
# Author: Alexander Soderqvist  <alexander.soderqvist@cosylab.com>

# The purpose of this template is to provide an interface to the ESS timing receiver.
# The actual timing receiver logic still has to be provided by an external component,
# which can be found in the trAction.template of the timing receiver support.
#
# It also handles auto abort for fly scan which is related to turning of
# trigger lines at the correct moment to assure conformity in positioner and data points.
#
# This template is not meant to be used as a stand-alone component but together with scanGeneric.template.

# Controls trigger generation on trigger liens on the timing receiver.
#
#
# MACRO:
# 	TR_TL1		Record Name of the TR record responsible to control the output
# 					Example: DTL-Ctrl:TR-01:ACT-TL3-EV5
# And TR_TL2 and TR_TL3 for controlling more trigger lines.
#
#	TL_ENBL_VAL	Value written to enable reaction to events on triggler line.
#	TL_DIS_VAL	Value written to disable reaction to events on triggler line.


# INTERFACE SSCAN record detector trigger:
# Detects the first detector trigger issued by SSCAN record (when <SSCAN>.CPT == 0)
# And activate trigger lines
record(calc, "$(DEVICE):$(SCAN)-TRTRG")
{
	field(DESC, "Detects first detector trigger")
	field(CALC, "A = 0 ? 1 : 2")

	field(INPA, "$(DEVICE):$(SCAN).CPT NPP")
	
	field(FLNK,  "$(DEVICE):$(SCAN)-OPSEL")
}

record(seq, "$(DEVICE):$(SCAN)-OPSEL")
{
	field(SELM, "Specified")
	field(SELL, "$(DEVICE):$(SCAN)-TRTRG")

	# Enable trigger lines
	field(DO1, "$(TL_ENBL_VAL=1)")
	field(LNK1, "$(DEVICE):$(SCAN)-TR_TLCTRL.VAL PP")
	
	# Else dact trigger lines
	field(DO2, "1")
	field(LNK2, "$(DEVICE):$(SCAN)-DACT_TR.PROC PP")
}

# INTERNAL RECORD & INTERFACE SCAN GENERIC:
# Control Trigger lines reaction to event
# "Simultaneosly" enable/disable reaction to events on multiple trigger lines.
# Note that this might be a hack. The timing receiver has to support doing this otherwise
# we have a race condition, even though it seems unlikely to actually happen.
# This will however only happen for fly scans.
record(dfanout, "$(DEVICE):$(SCAN)-TR_TLCTRL")
{
	field(DESC, "Enable/disable TR output")
	field(OMSL, "supervisory")

	field(OUTA, "$(TR_TL1=)")
	field(OUTB, "$(TR_TL2=)")
	field(OUTC, "$(TR_TL3=)")
}

# INTERNAL RECORD:
# Deactivate trigger lines for fly scans when number of points 
# is reached or the positioner is done moving.
record(calcout, "$(DEVICE):$(SCAN)-DACT_TR")
{
	field(DESC, "Deactivate TR trigger lines")
	field(CALC, "A = 2 && (B = 1 || C = D) ? 0 : 1")
	field(INPA, "$(DEVICE):$(SCAN).P4SM NPP")
	field(INPB, "$(POS_DMOVLNK=0)")
	field(INPC, "$(DEVICE):$(SCAN).CPT NPP")
	field(INPD, "$(DEVICE):$(SCAN).NPTS NPP")
	
	field(OCAL, "$(TL_DIS_VAL=0)")
	field(OOPT, "When Zero")
	field(DOPT, "Use OCAL")
	field(OUT,  "$(DEVICE):$(SCAN)-TR_TLCTRL PP")
# Trigger Auto abort
	field(FLNK, "$(DEVICE):$(SCAN)-AUTOABORT")
}

# INTERNAL RECORD: 
# If triggler lines were deactivated and positioner is done moving
# trigger auto abort links to finish the scan. To avoid waiting for NPTS to be collected.
record(calcout, "$(DEVICE):$(SCAN)-AUTOABORT")
{
	field(DESC, "Auto abort fly scans")
	field(CALC, "A = 0 && B = 1 ? 0 : 1")	
	field(INPA, "$(DEVICE):$(SCAN)-DACT_TR NPP")
	field(INPB, "$(POS_DMOVLNK=0)")
	field(OOPT, "When Zero")

	field(OUT,  "$(DEVICE):$(SCAN)-AUTOABLNK.PROC PP")
}

# Auto abort links.
# Abort scan and execute user defined links for any other things
# that needs to be finsihsed or reset.
record(seq, "$(DEVICE):$(SCAN)-AUTOABLNK")
{
	field(DLY1, "0.1")
	field(DO1, "0")
	field(LNK1, "$(DEVICE):$(SCAN).EXSC NPP")
	
	field(DLY2, "0.1")
	field(DO2, "$(AUTOAB_DO1=)")
	field(LNK2, "$(AUTOAB_LNK1=)")
	
	field(DLY3, "0.1")
	field(DO3, "$(AUTOAB_DO2=)")
	field(LNK3, "$(AUTOAB_LNK2=)")

}
