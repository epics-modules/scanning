# File: scanBase.template
# Author: Alexander Soderqvist <alexander.soderqvist@cosylab.com>
#
# Template containing the base for a scanning application.
# This is not a scanning application by it self
# and although it can be used as stand alone component, 
# it is designed to be a part of scanning application implemented
# within geenricScan.substitutions.


# SSCAN record

record(sscan, "$(DEVICE):$(SCAN)")
{
	field(DESC, "$(SCAN)")
	
	field(MPTS,  "$(MPTS)")
	field(PASM,  "$(PASM="STAY")")
	field(NPTS,  "$(NPTS)") 
	field(FPTS,  "$(FPTS="FREEZE")")
	field(ATIME, "$(ATIME=0.1)")
	field(ACQT, "$(ACQT=SCALAR)")
	field(ACQM, "$(ACQM=NORMAL)")

	#Before scan start fields
	field(BSPV,  "$(BSPV="")")
	field(BSCD,  "$(BSCD=1)")

	#After scan start field-
	field(ASPV,  "$(ASPV="")")
	field(ASCD,  "$(ASCD=1)")

	#Detector triggers
	field(T1PV,  "$(T1PV="")")
	field(T2PV,  "$(T2PV="")")
	field(T3PV,  "$(T3PV="")")
	field(T4PV,  "$(T4PV="")")

	#Detectors
	field(D01PV, "$(D01PV="")")
	field(D02PV, "$(D02PV="")")
	field(D03PV, "$(D03PV="")")
	field(D04PV, "$(D04PV="")")
	field(D05PV, "$(D05PV="")")
	field(D06PV, "$(D06PV="")")
	field(D07PV, "$(D07PV="")")
	field(D08PV, "$(D08PV="")")
	field(D09PV, "$(D09PV="")")
	field(D10PV, "$(D10PV="")")
	field(D11PV, "$(D11PV="")")
	field(D12PV, "$(D12PV="")")
	field(D13PV, "$(D13PV="")")
	field(D14PV, "$(D14PV="")")
	field(D15PV, "$(D15PV="")")
	field(D16PV, "$(D16PV="")")
	field(D17PV, "$(D17PV="")")
	field(D18PV, "$(D18PV="")")
	field(D19PV, "$(D19PV="")")
	field(D20PV, "$(D20PV="")")
	field(D21PV, "$(D21PV="")")
	field(D22PV, "$(D22PV="")")
	field(D23PV, "$(D23PV="")")
	field(D24PV, "$(D24PV="")")
	field(D25PV, "$(D25PV="")")
	field(D26PV, "$(D26PV="")")
	field(D27PV, "$(D27PV="")")
	field(D28PV, "$(D28PV="")")
	field(D29PV, "$(D29PV="")")
	field(D30PV, "$(D30PV="")")
	field(D31PV, "$(D31PV="")")
	field(D32PV, "$(D32PV="")")
	field(D33PV, "$(D33PV="")")
	field(D34PV, "$(D34PV="")")
	field(D35PV, "$(D35PV="")")
	field(D36PV, "$(D36PV="")")
	field(D37PV, "$(D37PV="")")
	field(D38PV, "$(D38PV="")")
	field(D39PV, "$(D39PV="")")
	field(D40PV, "$(D40PV="")")
	field(D41PV, "$(D41PV="")")
	field(D42PV, "$(D42PV="")")
	field(D43PV, "$(D43PV="")")
	field(D44PV, "$(D44PV="")")
	field(D45PV, "$(D45PV="")")
	field(D46PV, "$(D46PV="")")
	field(D47PV, "$(D47PV="")")
	field(D48PV, "$(D48PV="")")
	field(D49PV, "$(D49PV="")")
	field(D50PV, "$(D50PV="")")
	field(D51PV, "$(D51PV="")")
	field(D52PV, "$(D52PV="")")
	field(D53PV, "$(D53PV="")")
	field(D54PV, "$(D54PV="")")
	field(D55PV, "$(D55PV="")")
	field(D56PV, "$(D56PV="")")
	field(D57PV, "$(D57PV="")")
	field(D58PV, "$(D58PV="")")
	field(D59PV, "$(D59PV="")")
	field(D60PV, "$(D60PV="")")
	field(D61PV, "$(D61PV="")")
	field(D62PV, "$(D62PV="")")
	field(D63PV, "$(D63PV="")")
	field(D64PV, "$(D64PV="")")
	field(D65PV, "$(D65PV="")")
	field(D66PV, "$(D66PV="")")
	field(D67PV, "$(D67PV="")")
	field(D68PV, "$(D68PV="")")
	field(D69PV, "$(D69PV="")")
	field(D70PV, "$(D70PV="")")

	#Positioner and step mode
	field(P1PV,  "$(P1PV="")")
	field(P1SM,  "$(P1SM="")")
	field(P2PV,  "$(P2PV="")")
	field(P2SM,  "$(P2SM="")")
	field(P3PV,  "$(P3PV="")")
	field(P3SM,  "$(P3SM="")")
	field(P4PV,  "$(P4PV="")")
	field(P4SM,  "$(P4SM="")")

	#Start, end and diff limit
	field(P1SP,  "$(P1SP="")")
	field(P1EP,  "$(P1EP="")")
	field(P2SP,  "$(P2SP="")")
	field(P2EP,  "$(P2EP="")")
	field(P3SP,  "$(P3SP="")")
	field(P3EP,  "$(P3EP="")")
	field(P4SP,  "$(P4SP="")")
	field(P4EP,  "$(P4EP="")")
	
	# Freeze flags, start, increment and end
	field(P1FS,  "$(P1FS="")")
	field(P1FI,  "$(P1FI="")")
	field(P1FE,  "$(P1FE="")")
	field(P2FS,  "$(P2FS="")")
	field(P2FI,  "$(P2FI="")")
	field(P2FE,  "$(P2FE="")")
	field(P3FS,  "$(P3FS="")")
	field(P3FI,  "$(P3FI="")")
	field(P3FE,  "$(P3FE="")")
	field(P4FS,  "$(P4FS="")")
	field(P4FI,  "$(P4FI="")")
	field(P4FE,  "$(P4FE="")")

	# Position readback PVs
	field(R1PV, "$(R1PV="")")
	field(R2PV, "$(R2PV="")")
	field(R3PV, "$(R3PV="")")
	field(R4PV, "$(R4PV="")")

	field(A1PV, "$(A1PV="")")
}
