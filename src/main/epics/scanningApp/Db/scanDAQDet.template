# scanDet.template
# Author: Alexander Soderqvist <alexander.soderqvist@esss.se>
#
# Generic detector for scan.
# Used with scanDetTrig.template in generic scanning.
# NOT SUPPOSED be used as a stand alone Component


# DAQ INTERFACE:
# Guard to avoid processing unless detector trigger on SSCAN notified busy
# This is the record where the DAQ input channels forward link should be set to.

record(calcout, "$(DEVICE):$(DET)-GUARD")
{
	field(CALC, "A")
	field(INPA, "$(DEVICE):$(DETTRG)-BUSY.VAL NPP")
	
	field(OOPT, "When Non-zero")
	field(DOPT, "Use OCAL")
	field(OCAL, "1")
	field(OUT,  "$(DEVICE):$(DET)-PULSCNT.PROC PP")
}

# INTERNAL RECORD:
# Incoming pulse acqusition counter

record(calcout, "$(DEVICE):$(DET)-PULSCNT")
{
	field(CALC, "C = 1 ? (A % ABS(B)) + 1 : 0")
	field(INPA, "$(DEVICE):$(DET)-PULSCNT NPP")
	field(INPB, "$(TRGRRBVREC) NPP")
	field(INPC, "$(DEVICE):$(DETTRG)-BUSY.VAL NPP")
	
	field(OOPT, "When Non-zero")
	field(DOPT, "Use OCAL")
	field(OCAL, "1")

	field(OUT, "$(PULSCNTFLNK=$(DEVICE):$(DET)-TRGCOMP).PROC PP")
}

# INTERNAL RECORD: 
# Trigger compression of waveform record.
# This can be combined with averaging, in that case it should first trigger averaging
# of the summarized waveform and then compression is triggered from that record.

record(calcout, "$(DEVICE):$(DET)-TRGCOMP")
{
	field(CALC, "A = ABS(B) ? 0 : 1")
	field(INPA, "$(DEVICE):$(DET)-PULSCNT NPP")
	field(INPB, "$(TRGRRBVREC) NPP")
	field(OOPT, "When Zero")
	field(OUT,  "$(TRGCOMPFLNK=$(DEVICE):$(DET)-COMPN).PROC PP")
}

# DETECTOR TRIGGER INTERFACE:
# Set compress record to averageover N elements.
# This should be Number of samples from the data acqusition device.
# There has to be a forward link to this record for nsampels setting on DAQ.

record(seq, "$(DEVICE):$(DET)-COMPN")
{
	field(DOL1, "$(WFNORD)")
	field(LNK1, "$(DEVICE):$(DET)-COMP.N")
	field(FLNK, "$(DEVICE):$(DET)-COMP")
}

# DETECTOR TRIGGER and sSCAN DETECTOR INTERFACE:
# Averages DAQ DATA array to a scalar value.
# Holds scalar value which should be referenced to by sscan's detector.

record(compress, "$(DEVICE):$(DET)-COMP")
{
	field(ALG,  "N to 1 Average")
	field(INP,  "$(WFSRCREC)")
	field(N,    "$(NELM)")
	field(FLNK, "$(DEVICE):$(DETTRG)-DETCNT")
}



# DETECTOR TRIGGER INTERFACE:
# Resets detectors pulse counter when an abort is issued.

record(bo, "$(DEVICE):$(DET)-RSTABORT")
{
	field(SCAN, "Event")
	field(EVNT, "$(RSTEVENT)")
	field(FLNK,  "$(DEVICE):$(DET)-PULSCNT")
}

# DAQ INTERFACE:
# Reset puls counter if Trigger repeat on DAQ changes

record(longin, "$(DEVICE):$(DET)-RSTTRGRCHG")
{
	field(INP, "$(TRGRRBVREC) CP")
	field(FLNK,  "$(DEVICE):$(DET)-PULSCNT")
}