# File: scan.substitutions
#
# Author: Rok Stefanic <rok.stefanic@cosylab.com>
# Modified: Alexander Soderqvist <alexander.soderqvist@esss.se>
#
# One dimensional scanning application with support for ESS timing receiver.
# The component can operate as a single component to perform a 1D scan,
# or as chained components for performing multiple dimension scans.
#
# This substitutions file serves as glue to combine the following individual components:
#    1. sscan.template    - template containing only SSCAN record
#    2. scanGeneric.templaet - template with actual glue logic, wrapper for sscan record.
#    3. scanTRDetTrig.template  - template responsible to acticate trigger lines on ESS timing receiver
#    4. scanTSArray.template - Template with timestamp array and some timestamp logic.
#    
#
# Connects to external positioner control db (for example PMAC motor control, voltage positioner etc.)
# Connects to external detector control db (for example PCI9116 DAQ card, SIS8300 etc.)
# 

#
# Scan base
#
	
file sscan.template
{
	pattern {
		DEVICE, 
		SCAN, 
		
		MPTS, 
		NPTS,
		
		BSPV, 
		ASPV,
		
		P1FS, 
		P1FE,

# INTERFACE: POSITIONER		
		P1PV,
		P1SM,
		R1PV,
		P2PV,
		P2SM,
		R2PV,
		P3PV,
		P3SM,
		R3PV,
		P4PV,
		P4SM,

# INTERFACE: DETECTOR TRIGGERS		
		T1PV, 
		T2PV,
		T3PV,
		T4PV,
		
		D01PV,
		D02PV,		
		D03PV,
		D04PV,
		D05PV,
		D06PV,
		D07PV,
		D08PV,
		D09PV,
		D10PV,
		D11PV,
		D12PV,
		D13PV,
		D14PV,
		D15PV,
		D16PV,
		D17PV,
		D18PV,
		D19PV,
		D20PV,
		D21PV,
		D22PV,
		D23PV,
		D24PV,
		D25PV,
		D26PV,
		D27PV,
		D28PV,
		D29PV,
		D30PV,
		D31PV,
		D32PV,
		D33PV,
		D34PV,
		D35PV,
		D36PV,
		D37PV,
		D38PV,
		D39PV,
		D40PV,
		D41PV,
		D42PV,
		D43PV,
		D44PV,
		D45PV,
		D46PV,
		D47PV,
		D48PV,
		D49PV,
		D50PV,
		D51PV,
		D52PV,
		D53PV,
		D54PV,
		D55PV,
		D56PV,
		D57PV,
		D58PV,
		D59PV,
		D60PV,
		D61PV,
		D62PV,
		D63PV,
		D64PV,
		D65PV,
		D66PV,
		D67PV,
		D68PV,
		D69PV,
		D70PV,

		A1PV
	}
	{
		$(DEVICE), 
		$(SCAN), 
		
		$(MAX_POINTS), 
		$(MAX_POINTS),

# Internal records in scanGeneric.template
		$(DEVICE):$(SCAN)-BEFORE_SCAN, 
		$(DEVICE):$(SCAN)-AFTER_SCAN, 

# SSCAN record initial settings
		"FREEZE",
		"FREEZE",

# INTERFACE: POSITIONER
# Positioner 1 is used as second derivate, i.e. if positioner allows setting acceleration.
# Positioner 2 as first derivative, i.e. if positioner allows setting speed/velocity.
# Positioner 3 is free to use as whatever necessary.
# Positioner 4 should be used as default positioner. I.e. the field commanding a motor to a certain positioner or field to command a PSU to set e certain voltage.

		\$(POS1_SP_PV=),
		\$(POS1_MODE=FLY),
		\$(POS1_RBV_PV=),
		\$(POS2_SP_PV=),
		\$(POS2_MODE=FLY),
		\$(POS2_RBV_PV=),
		\$(POS3_SP_PV=),
		\$(POS3_MODE=FLY),
		\$(POS3_RBV_PV=),
		$(POS_SP_PV),
		\$(POS_MODE=LINEAR),
		
# INTERFACE: DETECTOR TRIGGERS
# T4PV is taken by TRCTRL. Define T3PV-T4PV in scanBase if necessary.
		\$(T1PV=),	
		\$(T2PV=),
		\$(T3PV=),
		$(DEVICE):$(SCAN)-TRTRG.PROC,
# INTERFACE: DETECTORS
# D01PV to D70PV exist in sscan record, define more macros in scanBase if necessary.
		$(D01PV),
		\$(D02PV=),
		\$(D03PV=),
		\$(D04PV=),
		\$(D05PV=),
		\$(D06PV=),
		\$(D07PV=),
		\$(D08PV=),
		\$(D09PV=),
		\$(D10PV=),
		\$(D11PV=),
		\$(D12PV=),
		\$(D13PV=),
		\$(D14PV=),
		\$(D15PV=),
		\$(D16PV=),
		\$(D17PV=),
		\$(D18PV=),
		\$(D19PV=),
		\$(D20PV=),
		\$(D21PV=),
		\$(D22PV=),
		\$(D23PV=),
		\$(D24PV=),
		\$(D25PV=),
		\$(D26PV=),
		\$(D27PV=),
		\$(D28PV=),
		\$(D29PV=),
		\$(D30PV=),
		\$(D31PV=),
		\$(D32PV=),
		\$(D33PV=),
		\$(D34PV=),
		\$(D35PV=),
		\$(D36PV=),
		\$(D37PV=),
		\$(D38PV=),
		\$(D39PV=),
		\$(D40PV=),
		\$(D41PV=),
		\$(D42PV=),
		\$(D43PV=),
		\$(D44PV=),
		\$(D45PV=),
		\$(D46PV=),
		\$(D47PV=),
		\$(D48PV=),
		\$(D49PV=),
		\$(D50PV=),
		\$(D51PV=),
		\$(D52PV=),
		\$(D53PV=),
		\$(D54PV=),
		\$(D55PV=),
		\$(D56PV=),
		\$(D57PV=),
		\$(D58PV=),
		\$(D59PV=),
		\$(D60PV=),
		\$(D61PV=),
		\$(D62PV=),
		\$(D63PV=),
		\$(D64PV=),
		\$(D65PV=),
		\$(D66PV=),
		\$(D67PV=),
		\$(D68PV=),
		\$(D69PV=),
		\$(D70PV=),

		\$(A1PV=)
		
	}	
}		


#
# Scan control
#

file scanGeneric.template
{
	pattern {
		DEVICE,
		SCAN,
		
# INTERFACE: POSITIONERS
		POS_NUM
		POS_SCALAR_RBV_PV,
		POS_ARR_RBV_PV,
		POS_ARR_NORD_RBV_PV

		TL_DIS_VAL,
# INTERFACE: LINKS
  	 	SET_STEP_LNK1,
		SET_STEP_LNK2,
		SET_STEP_DO1,
		SET_STEP_DO2,

  	 	SET_FLY_LNK1,
		SET_FLY_LNK2,
		SET_FLY_DO1,
		SET_FLY_DO2,

		BS_LNK,
		BS_DO,

		AS_LNK,
		AS_DO,

		AB_LNK,
		AB_DO,
		
		BF_STEP_LNK1,
		BF_STEP_LNK2,
		BF_STEP_LNK3,
		BF_STEP_DO1,
		BF_STEP_DO2,
		BF_STEP_DO3,
		BF_STEP_FLNK,
		
		AF_STEP_LNK1,
		AF_STEP_LNK2,
		AF_STEP_LNK3,
		AF_STEP_DO1,
		AF_STEP_DO2,
		AF_STEP_DO3,
		AF_STEP_FLNK,		

		AB_STEP_LNK1,
		AB_STEP_LNK2,
		AB_STEP_LNK3,
		AB_STEP_DO1,
		AB_STEP_DO2,
		AB_STEP_DO3,
		AB_STEP_FLNK,
		
		BF_FLY_LNK1,
		BF_FLY_LNK2,
		BF_FLY_LNK3,
		BF_FLY_DO1,
		BF_FLY_DO2,
		BF_FLY_DO3,
		BF_FLY_FLNK,
		
		AF_FLY_LNK1,
		AF_FLY_LNK2,
		AF_FLY_LNK3,
		AF_FLY_DO1,
		AF_FLY_DO2,
		AF_FLY_DO3,
		AF_FLY_FLNK,
		
		AB_FLY_LNK1,
		AB_FLY_LNK2,
		AB_FLY_LNK3,
		AB_FLY_DO1,
		AB_FLY_DO2,
		AB_FLY_DO3,
		AB_FLY_FLNK		
	}
	{
		$(DEVICE),
		$(SCAN),
		
# INTERFACE: POSITIONER
# Positioner 4 is standard positioner command issuing movement
		4,
		$(POS_SCALAR_RBV_PV),
		\$(POS_ARR_RBV_PV=),
		\$(POS_ARR_NORD_RBV_PV=0)

# Disable value for triggerlines, needed for Abort	
		\$(TL_DIS_VAL=0),

# INTERFACE: LINKS
# Before setting step-by-step
  	 	\$(SET_STEP_LNK1=),
		\$(SET_STEP_LNK2=),
		\$(SET_STEP_DO1=),
		\$(SET_STEP_DO2=),

# Before setting fly
  	 	\$(SET_FLY_LNK1=),
		\$(SET_FLY_LNK2=),
		\$(SET_FLY_DO1=),
		\$(SET_FLY_DO2=),

# Before scan general
		\$(BS_LNK=),
		\$(BS_DO=),	

# After scan general		
		\$(AS_LNK=),
		\$(AS_DO=),

# Abort scan general
		\$(AB_LNK=),
		\$(AB_DO=),

# Before scan step-by-step
		\$(BF_STEP_LNK1=),
		\$(BF_STEP_LNK2=),
		\$(BF_STEP_LNK3=),
		\$(BF_STEP_DO1=),
		\$(BF_STEP_DO2=),
		\$(BF_STEP_DO3=),
		\$(BF_STEP_FLNK=),
		
# After scan step-by-step		
		\$(AF_STEP_LNK1=),
		\$(AF_STEP_LNK2=),
		\$(AF_STEP_LNK3=),
		\$(AF_STEP_DO1=),
		\$(AF_STEP_DO2=),
		\$(AF_STEP_DO3=),
		\$(AF_STEP_FLNK=),		
		
# Abort scan step-by-step		
		\$(AB_STEP_LNK1=),
		\$(AB_STEP_LNK2=),
		\$(AB_STEP_LNK3=),
		\$(AB_STEP_DO1=),
		\$(AB_STEP_DO2=),
		\$(AB_STEP_DO3=),
		\$(AB_STEP_FLNK=),

# Before scan fly		
		\$(BF_FLY_LNK1=),
		\$(BF_FLY_LNK2=),
		\$(BF_FLY_LNK3=),
		\$(BF_FLY_DO1=),
		\$(BF_FLY_DO2=),
		\$(BF_FLY_DO3=),
		\$(BF_FLY_FLNK=),		

# After scan fly		
		\$(AF_FLY_LNK1=),
		\$(AF_FLY_LNK2=),
		\$(AF_FLY_LNK3=),
		\$(AF_FLY_DO1=),
		\$(AF_FLY_DO2=),
		\$(AF_FLY_DO3=),
		\$(AF_FLY_FLNK=),		
		
# Abort scan fly				
		\$(AB_FLY_LNK1=),
		\$(AB_FLY_LNK2=),
		\$(AB_FLY_LNK3=),
		\$(AB_FLY_DO1=),
		\$(AB_FLY_DO2=),
		\$(AB_FLY_DO3=),
		\$(AB_FLY_FLNK=)
	}
}

#
# Time stamp array
#

file scanTSArray.template
{
	pattern { 
		DEVICE,
		SCAN,
		
		TR_TLTSEL,
		MAX_POINTS
	}
	{ 
		$(DEVICE),
		$(SCAN),
		
		$(TR_TLTSEL),
		$(MAX_POINTS)
	}
}

#
# Timing receiver trigger lines control
#

file scanTRDetTrig.template
{
	pattern { 
		DEVICE,
		SCAN,
		
		TR_TL1,
		TR_TL2,
		TR_TL3,

		TL_ENBL_VAL,
		TL_DIS_VAL,

		POS_DMOVLNK
		AUTOAB_DO1,
		AUTOAB_LNK1,
		AUTOAB_DO2,
		AUTOAB_LNK2
	}
	{ 
		$(DEVICE),
		$(SCAN),
		
		\$(TR_TL1=),
		\$(TR_TL2=),
		\$(TR_TL3=),

		\$(TL_ENBL_VAL=1),
		\$(TL_DIS_VAL=0),
		
		\$(POS_DMOVLNK=0),

		\$(AUTOAB_DO1=),
		\$(AUTOAB_LNK1=),
		\$(AUTOAB_DO2=),
		\$(AUTOAB_LNK2=)
	}
}
