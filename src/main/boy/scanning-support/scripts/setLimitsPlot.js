importPackage(Packages.org.csstudio.opibuilder.scriptUtil);

var min = PVUtil.getDouble(pvArray[0]);
var max = PVUtil.getDouble(pvArray[1]);
widgetController.setPropertyValue("axis_0_minimum", min);
widgetController.setPropertyValue("axis_0_maximum", max);