importPackage(Packages.org.csstudio.opibuilder.scriptUtil);

var macroInput = DataUtil.createMacrosInput(true);
var prefix = PVUtil.getString(pvArray[0]);
var name = PVUtil.getString(pvArray[1]);
var det = PVUtil.getLong(pvArray[2]);

macroInput.put("DEVICE", prefix);
macroInput.put("Scan2Macro", name);
macroInput.put("Det", det);
widgetController.setPropertyValue("macros", macroInput);