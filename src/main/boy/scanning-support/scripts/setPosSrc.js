importPackage(Packages.org.csstudio.opibuilder.scriptUtil);

var macroInput = DataUtil.createMacrosInput(true);
macroInput = widgetController.getPropertyValue("macros");

var mode = PVUtil.getLong(pvArray[0]);
var flysrc = PVUtil.getString(pvArray[1]);
var defsrc = pvArray[2].name;

if (mode == 2) {
macroInput.put("PVX", flysrc);
}else{
macroInput.put("PVX", defsrc);
}

widgetController.setPropertyValue("macros", macroInput);
widget.setPropertyValue("opi_file","");
widget.setPropertyValue("opi_file","scanning-plot.opi");
