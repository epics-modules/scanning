importPackage(Packages.org.csstudio.opibuilder.scriptUtil);

if(typeof pvArray[0] === 'undefined' || typeof pvArray[1] === 'undefined'){
} 
else {
	var pv0 = PVUtil.getString(pvArray[0]);
	var pv1 = +PVUtil.getString(pvArray[1]);

	widget.setPropertyValue("tooltip",pv0);

	if(pv1 == 1){
		widget.setPropertyValue("enabled",false);
		widget.setPropertyValue("background_color","IO PV OFF");
	}
	else{
		widget.setPropertyValue("enabled",true);
		widget.setPropertyValue("background_color","IO Grid");
	}
}